# SIG Beauty - Sistema Gerenciador de Procedimentos de Beleza

O SIG Beauty é um Sistema Gerenciador de Procedimentos de Beleza que tem o objetivo de automatizar, facilitar e dar mais segurança no processo de gerenciamento de dados e informações realizado por gerentes ou administradores de salões de beleza. Esse projeto foi desenvolvido 
em Java como método avaliativo da disciplina de *Linguagem de Programação II* no semestre 2021.1.

## Compilação

### Usando linha de comando
Para o uso de linha de comando para compilação execute o comando abaixo para cada uma das classes presentes no diretório src.

```
javac <nomeDaClasse>
```

Então execute o seguinte comando.
```
java TelaPrincipal
``` 

### Usando Eclipse

Para executar o programa usando a IDE abra os arquivos na plataforma e use a opção `run as` e depois a opção `Java Application`na classe de visão `TelaPrincipal.java`.

### Usando arquivo .jar

Usando o arquivo `SIGBeauty.jar` use o comando abaixo no diretório em que se encontra o arquivo.

```
java -jar SIGBeauty.jar
```

## Instruções de uso

O programa funciona como um Sistema Gerenciador Procedimentos de Beleza e foi desenvolvido com o uso do **Eclipse IDE**, na **versão 11 do Java**.

# Autoria

Programa desenvolvido por **Alaide Lisandra Melo Carvalho** (<mendie73@gmail.com>) e **Mariane Felix Fernandes** (<marianefelix59@gmail.com>) como projeto para a disciplina de *Linguagem de Programação II* de 2021.1.

&copy; IMD/UFRN 2021.
