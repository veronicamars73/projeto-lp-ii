package br.ufrn.imd.modelo;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

import br.ufrn.imd.utilitarios.DataUtilitarios;

public class Banco {
	private ArrayList<Cliente> clientes;
	private ArrayList<Profissional> profissionais;
	private ArrayList<Procedimento> procedimentos;
	private ArrayList<Agendamento> agendamentos;
	
	private static Banco banco;
	
	private Banco(){
		this.clientes = new ArrayList<Cliente>();
		this.profissionais = new ArrayList<Profissional>();
		this.procedimentos = new ArrayList<Procedimento>();
		this.agendamentos = new ArrayList<Agendamento>();
	}
	

	public static Banco getInstance() {
		if (banco == null) {
			banco = new Banco();
		}

		return banco;
	}
	
	public String addCliente(Cliente cliente) {
		int quantidadeClientes = clientes.size();
		cliente.setId(quantidadeClientes + 1);
		
		clientes.add(cliente);
		return "Cliente cadastrado com sucesso!";
	}

	public String addProfissional(Profissional profissional) {
		int quantidadeProfissionais = profissionais.size();
		profissional.setId(quantidadeProfissionais + 1);
		
		profissionais.add(profissional);
		return "Profissional cadastrado com sucesso!";
	}

	public String addProcedimento(Procedimento procedimento) {
		int quantidadeProcedimento = procedimentos.size();
		procedimento.setId(quantidadeProcedimento + 1);
		
		procedimentos.add(procedimento);
		return "Procedimento cadastrado com sucesso!";
	}

	public String addAgendamento(Agendamento agendamento) {
		int quantidadeAgendamentos = agendamentos.size();
		agendamento.setId(quantidadeAgendamentos + 1);
		
		agendamentos.add(agendamento);
		atualizaCliente(agendamento.getClienteId(),agendamento.getDataHora());
		return "Agendamento cadastrado com sucesso!";
	}
	
	public void atualizaCliente(int clienteId, Date dataAgendamento) {
		for (int i= 0; i < this.clientes.size(); i++) {
			if (clientes.get(i).getId() == clienteId) {
				try {
					Date dataUltimoProcedimento = clientes.get(i).getDataUltimoProcedimento();

					if(dataUltimoProcedimento.before(dataAgendamento)) {
						(clientes.get(i)).setDataUltimoProcedimento(dataAgendamento);
					}
				} catch (NullPointerException nexc) {
					clientes.get(i).setDataUltimoProcedimento(dataAgendamento);
		        }
				
				int quantidadeProcedimentos = clientes.get(i).getQuantidadeProcedimentos() + 1;
			
				clientes.get(i).setQuantidadeProcedimentos(quantidadeProcedimentos);
				break;
			}
		}
	}
	
	public void atualizaClientePosRemocao(int clienteId) {
		Date ultimoAgendamento = null;

		for (int i=0; i<agendamentos.size();i++) {
			if (agendamentos.get(i).getClienteId() == clienteId) {
				try {
					if(ultimoAgendamento.before((agendamentos.get(i)).getDataHora())) {
						ultimoAgendamento = (agendamentos.get(i)).getDataHora();
					}
				}catch (NullPointerException nexc){
					ultimoAgendamento = (agendamentos.get(i)).getDataHora();
		        }
			}
		}

		for (int i = 0; i < this.clientes.size(); i++) {
			if (clientes.get(i).getId() == clienteId) {
				clientes.get(i).setDataUltimoProcedimento(ultimoAgendamento);
				clientes.get(i).setQuantidadeProcedimentos(clientes.get(i).getQuantidadeProcedimentos() - 1);
				break;
			}
		}
	}
	
	public ArrayList<Cliente> listaClientes() {
		return this.clientes;
	}
	
	public ArrayList<Cliente> listaClientesAtivos() {
		ArrayList<Cliente> clientesAtivos = new ArrayList<Cliente>();
		
		for(Cliente cliente : this.clientes) {
			if(cliente.EAtivo()) {
				clientesAtivos.add(cliente);
			}
		}

		return clientesAtivos;
	}
	
	public ArrayList<Cliente> listaClientesBonificados() {
		ArrayList<Cliente> clientesBonificados = new ArrayList<Cliente>();

		for(Cliente cliente : this.clientes) {
			if(cliente.recebeBonificacao()) {
				clientesBonificados.add(cliente);
			}
		}

		return clientesBonificados;
	}

	public ArrayList<Profissional> listaProfissionais() {
		return this.profissionais;
	}
	
	public ArrayList<Profissional> listaProfissionaisBonificados() {
		ArrayList<Profissional> profissionaisBonificados = new ArrayList<Profissional>();

		for(Profissional profissional : this.profissionais) {
			if(profissional.recebeBonificacao()) {
				profissionaisBonificados.add(profissional);
			}
		}
		return profissionaisBonificados;
	}
	
	public ArrayList<Profissional> listaProfissionaisAtivos() {
		ArrayList<Profissional> profissionaisAtivos = new ArrayList<Profissional>();

		for(Profissional profissional : this.profissionais) {
			if(profissional.EAtivo()) {
				profissionaisAtivos.add(profissional);
			}
		}
		return profissionaisAtivos;
	}
	
	public ArrayList<Procedimento> listaProcedimentos() {
		return this.procedimentos;
	}

	public ArrayList<Agendamento> listaAgendamentosDoDia() {
		ArrayList<Agendamento> agendamentosDiarios = new ArrayList<Agendamento>();
		Date dataAtual = new Date();
		String dataAtualFormatada = DataUtilitarios.converteDataParaString(dataAtual);

		for (Agendamento agendamento: agendamentos) {
			String dataAgendamento = DataUtilitarios.converteDataParaString(agendamento.getDataHora());

			if (dataAgendamento.equals(dataAtualFormatada)) {
				agendamentosDiarios.add(agendamento);
			}
		}
		
		return agendamentosDiarios;
	}
	
	public ArrayList<Agendamento> listaProximosAgendamentos() throws ParseException {
		ArrayList<Agendamento> agendamentosProximos = new ArrayList<Agendamento>();
		Date dataAtual = new Date();
		String dataAtualFormatada = DataUtilitarios.converteDataParaString(dataAtual);

		for (Agendamento agendamento: agendamentos) {
			String dataAgendamento = DataUtilitarios.converteDataParaString(agendamento.getDataHora());
			Date dataAtualSemHora = DataUtilitarios.converteStringParaData(dataAtualFormatada);
			Date dataAgendamentoSemHora = DataUtilitarios.converteStringParaData(dataAgendamento);

			if (dataAgendamentoSemHora.after(dataAtualSemHora) || dataAgendamento.equals(dataAtualFormatada)) {
				agendamentosProximos.add(agendamento);
			}
		}
		
		return agendamentosProximos;
	}
	
	public ArrayList<Agendamento> listaAgendamentos() {
		return this.agendamentos;
	}
	
	public Procedimento buscaProcedimento(String nomeProcedimento) {
		for (int i = 0; i < procedimentos.size(); i++) {
			if (nomeProcedimento.equals(procedimentos.get(i).getNome())) {
				return procedimentos.get(i);
			}
		}

		return null;
	}
	
	public Procedimento buscaProcedimento(int procedimentoId) {
		for (int i = 0; i < procedimentos.size(); i++) {
			if (procedimentoId == procedimentos.get(i).getId()) {
				return procedimentos.get(i);
			}
		}

		return null;
	}
	
	public Cliente buscaCliente(int clienteId) {
		for (int i = 0; i < clientes.size(); i++) {
			if (clienteId == (clientes.get(i)).getId()) {
				return clientes.get(i);
			}
		}

		return null;
	}
	
	public Profissional buscaProfissional(int profissionalId) {
		for (int i = 0; i < profissionais.size(); i++) {
			if (profissionalId == profissionais.get(i).getId()) {
				return profissionais.get(i);
			}
		}

		return null;
	}
	
	public Agendamento buscaAgendamento(int agendamentoId) {
		for (int i = 0; i < agendamentos.size(); i++) {
			if (agendamentoId == agendamentos.get(i).getId()) {
				return agendamentos.get(i);
			}
		}
		return null;
	}
	
	public String editaProcedimento(Procedimento novoProcedimento) {
		int novoProcedimentoId = novoProcedimento.getId();
		boolean procedimentoAtualizado = false;

		for (int i = 0; i < procedimentos.size(); i++) {
			int procedimentoId = procedimentos.get(i).getId();

			if (novoProcedimentoId == procedimentoId) {
				procedimentos.set(i, novoProcedimento);
				procedimentoAtualizado = true;
			}
		}
		
		if (procedimentoAtualizado) {
			return "Procedimento editado com sucesso!";
		} 
		return "Procedimento n�o encontrado";
	}

	public String editaAgendamento(Agendamento novoAgendamento) {
		int novoAgendamentoId = novoAgendamento.getId();
		boolean agendamentoAtualizado = false;

		for (int i = 0; i < agendamentos.size(); i++) {
			int agendamentoId = agendamentos.get(i).getId();

			if (novoAgendamentoId == agendamentoId) {
				int id_cliente_atual = agendamentos.get(i).getClienteId();
				agendamentos.set(i, novoAgendamento);
				if (id_cliente_atual!=agendamentos.get(i).getClienteId()) {
					atualizaClientePosRemocao(id_cliente_atual);
				}
				atualizaCliente(novoAgendamento.getClienteId(),novoAgendamento.getDataHora());
				agendamentoAtualizado = true;
				break;
			}
		}
		
		if (agendamentoAtualizado) {
			return "Agendamento editado com sucesso!";
		} 
		return "Agendamento n�o encontrado";
	}
	
	public String removeProcedimento(int procedimentoId) {
		boolean procedimentoRemovido = false;

		for (int i = 0; i < procedimentos.size(); i++) {
			if (procedimentoId == procedimentos.get(i).getId()) {
				procedimentos.remove(i);
				procedimentoRemovido = true;
			}
		}
		
		if (procedimentoRemovido) {
			return "Procedimento removido com sucesso!";
		} else {
			return "Procedimento n�o encontrado";
		}
	}

	public String removeAgendamento(int agendamentoId) {
		boolean agendamentoRemovido = false;
		int idcliente = 0;

		for (int i = 0; i < agendamentos.size(); i++) {
			if (agendamentoId == agendamentos.get(i).getId()) {
				idcliente = agendamentos.get(i).getClienteId();
				agendamentos.remove(i);
				agendamentoRemovido = true;
			}
		}
		
		if (agendamentoRemovido) {
			this.atualizaClientePosRemocao(idcliente);
			return "Agendamento removido com sucesso!";
		} else {
			return "Agendamento n�o encontrado";
		}
	}
}
