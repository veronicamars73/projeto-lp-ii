package br.ufrn.imd.modelo;

import java.text.ParseException; 
import java.util.ArrayList;
import java.util.Date;

import br.ufrn.imd.utilitarios.DataUtilitarios;

public class Profissional extends Pessoa implements Bonificacao {
	private Date dataInicioTrabalho;
	private ArrayList<Procedimento> procedimentosAplicaveis;
	
	public Profissional() {
		this.dataInicioTrabalho = null;
		this.procedimentosAplicaveis = new ArrayList<Procedimento>();
	}

	public Date getDataInicioTrabalho() {
		return dataInicioTrabalho;
	}

	public void setDataInicioTrabalho(String dataInicio) throws ParseException {
		this.dataInicioTrabalho = DataUtilitarios.converteStringParaData(dataInicio);
	}

	public ArrayList<Procedimento> getProcedimentosAplicaveis() {
		return procedimentosAplicaveis;
	}

	public void addProcedimentosAplicaveis(Procedimento procedimento) {
		this.procedimentosAplicaveis.add(procedimento);
	}

	@Override
	public boolean recebeBonificacao() {
		if (DataUtilitarios.eAnteriorAPeriodo(dataInicioTrabalho, 12)) {
        	return true;
        }
		return false;
	}
}
