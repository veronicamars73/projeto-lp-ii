package br.ufrn.imd.modelo;

import java.text.ParseException;
import java.util.Date;

import br.ufrn.imd.utilitarios.DataUtilitarios;

public class Agendamento {
	private int id = 0;
	private Date dataHora;
	private int procedimentoId = 0;
	private int clienteId = 0;
	private int profissionalId = 0;
	
	public Agendamento () {
		this.dataHora = null;
	}

	public Date getDataHora() {
		return dataHora;
	}

	public void setDataHora(String data, String hora) throws ParseException {	
		this.dataHora = DataUtilitarios.converteStringParaData(data, hora);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getProcedimentoId() {
		return procedimentoId;
	}

	public void setProcedimentoId(int procedimentoId) {
		this.procedimentoId = procedimentoId;
	}

	public int getClienteId() {
		return clienteId;
	}

	public void setClienteId(int clienteId) {
		this.clienteId = clienteId;
	}

	public int getProfissionalId() {
		return profissionalId;
	}

	public void setProfissionalId(int profissionalId) {
		this.profissionalId = profissionalId;
	}
}
