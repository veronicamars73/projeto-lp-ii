package br.ufrn.imd.modelo;

public class Procedimento {
	private int id = 0;
	private String nome;
	private int duracaoMinutos;
	private double valor;
	
	public Procedimento() {
		this.nome = "";
		this.duracaoMinutos = 0;
		this.valor = 0.0;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getDuracaoMinutos() {
		return duracaoMinutos;
	}

	public void setDuracaoMinutos(int duracaoMinutos) {
		this.duracaoMinutos = duracaoMinutos;
	}

	public double getValor() {
		return valor;
	}

	public void setValor(double valor) {
		this.valor = valor;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
