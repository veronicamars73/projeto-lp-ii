package br.ufrn.imd.modelo;

import java.util.Date;

import br.ufrn.imd.utilitarios.DataUtilitarios;

public class Cliente extends Pessoa implements Bonificacao {
	private String endereco;
	private Date dataUltimoProcedimento;
	private int quantidadeProcedimentos;
	
	public Cliente() {
		this.endereco = "";
		this.dataUltimoProcedimento = null;
		this.quantidadeProcedimentos = 0;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public Date getDataUltimoProcedimento() {
		return dataUltimoProcedimento;
	}

	public void setDataUltimoProcedimento(Date dataUltimoProcedimento) {
		this.dataUltimoProcedimento = dataUltimoProcedimento;
	}

	public int getQuantidadeProcedimentos() {
		return quantidadeProcedimentos;
	}

	public void setQuantidadeProcedimentos(int quantidadeProcedimentos) {
		this.quantidadeProcedimentos = quantidadeProcedimentos;
	}
	
	@Override
	public boolean recebeBonificacao() {
		boolean quantidadeEMultiploDeDez = (this.quantidadeProcedimentos % 10) == 0;
		
		if (quantidadeEMultiploDeDez && this.quantidadeProcedimentos!=0) {
			return true;
		}
		return false;
	}
	
	public boolean EAtivo() {
		Date dataUltimoProcedimento = this.dataUltimoProcedimento;
		
		if (dataUltimoProcedimento != null && DataUtilitarios.eAnteriorAPeriodo(dataUltimoProcedimento, 3)) {
			this.ativo = false;
		} else {
			this.ativo = true;
		}
		
		return this.ativo;
	}
}
