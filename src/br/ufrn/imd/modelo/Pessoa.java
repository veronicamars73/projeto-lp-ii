package br.ufrn.imd.modelo;

public abstract class Pessoa {
	private int id = 0;
	private String nome;
	private String contato;
	protected boolean ativo;
	
	public Pessoa() {
		this.nome = "";
		this.contato = "";
		this.ativo = true;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getContato() {
		return contato;
	}

	public void setContato(String contato) {
		this.contato = contato;
	}

	public boolean EAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
