package br.ufrn.imd.visao;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import br.ufrn.imd.modelo.Banco;
import br.ufrn.imd.modelo.Procedimento;

public class TelaCadastroProcedimento extends JInternalFrame implements ActionListener{
	
	private static final long serialVersionUID = 1L;

	Banco bc;
	
	// Format as datas
	SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
	
	// r�tulos
	JLabel lnome = new JLabel("Nome:");
	JLabel lduracaoMinutos = new JLabel("Dura��o (min):");
	JLabel lvalor = new JLabel("Valor:");
			
	// campos
	JTextField tnome = new JTextField();
	JTextField tduracaoMinutos = new JTextField();
	JTextField tvalor = new JTextField();
			
	// bot�es
	JButton btSubmeter = new JButton("Cadastrar");
	JButton btLimpar = new JButton("Limpar");

	public TelaCadastroProcedimento(String str)  {
		super(str,false,true);
		
		Container ct = this.getContentPane();
		ct.setLayout(null);
		
			
		// coordenadas
		lnome.setBounds(10,10,100,30);
		tnome.setBounds(100,10,280,25);
		lduracaoMinutos.setBounds(10,40,100,30);
		tduracaoMinutos.setBounds(100,40,50,25);
		lvalor.setBounds(10,70,100,30);
		tvalor.setBounds(100,70,50,25);

		// idem
		btSubmeter.setBounds(50,140,100,30);
		btLimpar.setBounds(230,140,100,30);
				
		// adicionando componentes
		ct.add(lnome);
		ct.add(tnome);
		ct.add(lduracaoMinutos);
		ct.add(tduracaoMinutos);
		ct.add(lvalor);
		ct.add(tvalor);
		ct.add(btSubmeter);
		ct.add(btLimpar);
		
		// evento dos bot�es
		btSubmeter.addActionListener(this);		
		btLimpar.addActionListener(this);
		
		// especifica��es do formul�rio
		setSize(785, 600);
		setTitle(str);
		
		bc = Banco.getInstance();
	}
	
	public void limpaCampos() {
		tnome.setText("");
		tduracaoMinutos.setText("");
		tvalor.setText("");
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btSubmeter) {
			Procedimento p = new Procedimento();
			boolean deveAdd = true;
			try {
				p.setNome(tnome.getText());
				p.setDuracaoMinutos(Integer.parseInt(tduracaoMinutos.getText()));
				p.setValor(Double.parseDouble(tvalor.getText()));
			} catch (Exception ept) {
				deveAdd = false;
				JOptionPane.showMessageDialog(this, "Existem campos com valores inv�lidos");
			}

			if (deveAdd) {
				String mensagemSucesso = bc.addProcedimento(p);
				JOptionPane.showMessageDialog(this, mensagemSucesso);

				if (JOptionPane.OK_OPTION != 1) {
					this.limpaCampos();
				}
			}
		} else if (e.getSource() == btLimpar) {
			this.limpaCampos();
		}
	}
}


