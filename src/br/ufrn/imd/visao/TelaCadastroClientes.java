package br.ufrn.imd.visao;
import javax.swing.JOptionPane;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import br.ufrn.imd.modelo.Banco;
import br.ufrn.imd.modelo.Cliente;

public class TelaCadastroClientes extends JInternalFrame implements ActionListener{
	
	private static final long serialVersionUID = 1L;

	Banco bancoDeDados;
	
	JLabel rotuloNome = new JLabel("Nome:");
	JLabel rotuloContato = new JLabel("Contato:");
	JLabel rotuloEndereco = new JLabel("Endere�o:");
			
	JTextField campoNome = new JTextField();
	JTextField campoContato = new JTextField();
	JTextField campoEndereco = new JTextField();
			
	JButton botaoSubmeter = new JButton("Cadastrar");
	JButton botaoLimpar = new JButton("Limpar");
	
	JOptionPane optionPane = new JOptionPane();
	Container container = this.getContentPane();

	public TelaCadastroClientes(String valor)  {
		super(valor, false, true);
		
		container.setLayout(null);
		
			
		// coordenadas
		rotuloNome.setBounds(10,10,100,30);
		campoNome.setBounds(92,10,280,25);
		rotuloContato.setBounds(10,40,100,30);
		campoContato.setBounds(92,40,100,25);
		rotuloEndereco.setBounds(10,70,100,30);
		campoEndereco.setBounds(92,70,280,25);

		// idem
		botaoSubmeter.setBounds(50,140,100,30);
		botaoLimpar.setBounds(230,140,100,30);
				
		// adicionando componentes
		container.add(rotuloNome);
		container.add(campoNome);
		container.add(rotuloContato);
		container.add(campoContato);
		container.add(rotuloEndereco);
		container.add(campoEndereco);
		container.add(botaoSubmeter);
		container.add(botaoLimpar);
		
		// evento dos bot�es
		botaoSubmeter.addActionListener(this);		
		botaoLimpar.addActionListener(this);
		
		// especifica��es do formul�rio
		setSize(785, 600);
		setTitle(valor);
		
		bancoDeDados = Banco.getInstance();
	}
	
	public void limpaCampos() {
		campoNome.setText("");
		campoContato.setText("");
		campoEndereco.setText("");
	}
	
	public boolean camposSaoValidos() {
		boolean valido = true;
		String mensagemErro = "";

		if (campoNome.getText().equals("")) {
			valido = false;
			mensagemErro = "O campo Nome n�o pode ser vazio \n";
		}
		
		if (campoContato.getText().equals("")) {
			valido = false;
			mensagemErro += "O campo Contato n�o pode ser vazio \n";
		}

		if (campoEndereco.getText().equals("")) {
			valido = false;
			mensagemErro += "O campo Endere�o n�o pode ser vazio \n";
		}
		
		if (!mensagemErro.equals("")) {
			JOptionPane.showMessageDialog(this, mensagemErro);
		}
		
		return valido;
	}
	
	@Override
	public void actionPerformed(ActionEvent evento) {
		if (evento.getSource() == botaoSubmeter && camposSaoValidos()) {
			Cliente novoCliente = new Cliente();
			
			novoCliente.setNome(campoNome.getText());
			novoCliente.setContato(campoContato.getText());
			novoCliente.setEndereco(campoEndereco.getText());
			
			String mensagemSucesso = bancoDeDados.addCliente(novoCliente);
			JOptionPane.showMessageDialog(this, mensagemSucesso);

			if (JOptionPane.OK_OPTION != 1) {
				this.limpaCampos();
			}
		} else if (evento.getSource() == botaoLimpar) {
			this.limpaCampos();
		}
	}

}

