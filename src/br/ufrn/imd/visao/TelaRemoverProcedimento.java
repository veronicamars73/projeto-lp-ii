package br.ufrn.imd.visao;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import br.ufrn.imd.modelo.Banco;
import br.ufrn.imd.modelo.Procedimento;

public class TelaRemoverProcedimento extends JInternalFrame implements ActionListener{
	
	private static final long serialVersionUID = 1L;
	
	Banco bc;
	
	// Format as datas
	SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
	
	// r�tulos
	JLabel lid = new JLabel("Id Proce.:");
	JLabel lnome = new JLabel("Nome:");
	JLabel lduracaoMinutos = new JLabel("Dura��o (min):");
	JLabel lvalor = new JLabel("Valor:");
			
	// campos
	JTextField tid = new JTextField();
			
	// bot�es
	JButton btBuscaId = new JButton("Buscar Id");
	JButton btSubmeter = new JButton("Deletar");

	public TelaRemoverProcedimento(String str)  {
		super(str,false,true);
		
		Container ct = this.getContentPane();
		ct.setLayout(null);
		
			
		// coordenadas
		lid.setBounds(10,10,100,30);
		tid.setBounds(100,10,50,25);
		lnome.setBounds(10,40,200,30);
		lduracaoMinutos.setBounds(10,70,200,30);
		lvalor.setBounds(10,100,200,30);

		btBuscaId.setBounds(180,10,90,20);
		btSubmeter.setBounds(150,140,100,30);
				
		// adicionando componentes
		ct.add(lid);
		ct.add(tid);
		ct.add(lnome);
		ct.add(lduracaoMinutos);
		ct.add(lvalor);
		ct.add(btBuscaId);
		ct.add(btSubmeter);
		
		// evento dos bot�es
		btSubmeter.addActionListener(this);	
		btBuscaId.addActionListener(this);
		
		// especifica��es do formul�rio
		setSize(390,210);
		setTitle(str);
		
		bc = Banco.getInstance();
	}
	

	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btSubmeter){
			String mensagemSucesso = bc.removeProcedimento(Integer.parseInt(tid.getText()));
			
			JOptionPane.showMessageDialog(this, mensagemSucesso);

			if (JOptionPane.OK_OPTION != 1) {
				tid.setText("");
			}
		} else if(e.getSource() == btBuscaId){
			Procedimento pAux = bc.buscaProcedimento(Integer.parseInt(tid.getText()));
			if (pAux == null) {
				JOptionPane.showMessageDialog(this, "Procedimento n�o encontrado");
				lnome.setText("Nome: ");
				lduracaoMinutos.setText("Dura��o (min): ");
				lvalor.setText("Valor: ");
			} else {
				lnome.setText("Nome: " + pAux.getNome());
				lduracaoMinutos.setText("Dura��o (min): " + Integer.toString(pAux.getDuracaoMinutos()));
				lvalor.setText("Valor: " + Double.toString(pAux.getValor()));
			}
		}
	}
}


