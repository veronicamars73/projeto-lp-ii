package br.ufrn.imd.visao;

import java.awt.Container; 
import java.awt.FlowLayout;
import java.text.ParseException;
import java.util.ArrayList;

import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import br.ufrn.imd.modelo.Agendamento;
import br.ufrn.imd.modelo.Banco;
import br.ufrn.imd.utilitarios.DataUtilitarios;

public class TelaListaAgendamentos extends JInternalFrame {
	private static final long serialVersionUID = 1L;
	Banco bancoDeDados;

	TelaListaAgendamentos(String titulo, int tipoListagem) {
		super(titulo, false, true);

		bancoDeDados = Banco.getInstance();

		String colunas[] = { "ID", "Data", "Hora", "Procedimento", "Cliente", "Profissional" };

		ArrayList<Agendamento> agendamentos;
		switch (tipoListagem) {
			case 1:
				agendamentos = bancoDeDados.listaAgendamentosDoDia();
				break;
			case 2:
				try {
					agendamentos = bancoDeDados.listaProximosAgendamentos();
				} catch (ParseException e) {
					agendamentos = null;
					JOptionPane.showMessageDialog(this, "Algo deu errado. Tente novamente mais tarde.");
				}
				break;
			default:
				agendamentos = bancoDeDados.listaAgendamentos();
				break;
		}

		String listaAgendamentos[][] = new String[agendamentos.size()][6];
		int contador = 0;

		for (Agendamento agendamento : agendamentos) {
			String agendamentoDiario[] = new String[6];

			agendamentoDiario[0] = Integer.toString(agendamento.getId());

			String data = DataUtilitarios.converteDataParaString(agendamento.getDataHora());
			agendamentoDiario[1] = data;

			String hora = DataUtilitarios.converteDataHoraParaString(agendamento.getDataHora()).split(" ")[1];
			agendamentoDiario[2] = hora;

			String nomeProcedimento = bancoDeDados.buscaProcedimento(agendamento.getProcedimentoId()).getNome();
			agendamentoDiario[3] = nomeProcedimento;

			String nomeCliente = bancoDeDados.buscaCliente(agendamento.getClienteId()).getNome();
			agendamentoDiario[4] = nomeCliente;

			String nomeProfissional = bancoDeDados.buscaProfissional(agendamento.getProfissionalId()).getNome();
			agendamentoDiario[5] = nomeProfissional;

			listaAgendamentos[contador] = agendamentoDiario;
			contador += 1;
		}

		JTable table = new JTable(listaAgendamentos, colunas);
		JScrollPane scrollPane = new JScrollPane(table);

		scrollPane.setBounds(30, 40, 100, 100);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		table.setFillsViewportHeight(true);

		Container container = this.getContentPane();
		container.setLayout(new FlowLayout());
		container.add(scrollPane);

		// especificações do formulário
		setSize(785, 600);
		setTitle(titulo);
	}
}
