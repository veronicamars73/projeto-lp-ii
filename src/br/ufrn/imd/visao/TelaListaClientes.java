package br.ufrn.imd.visao;

import java.awt.Container;
import java.awt.FlowLayout;
import java.util.ArrayList;

import javax.swing.JInternalFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import br.ufrn.imd.modelo.Banco;
import br.ufrn.imd.modelo.Cliente;
import br.ufrn.imd.utilitarios.DataUtilitarios;

public class TelaListaClientes extends JInternalFrame {

	private static final long serialVersionUID = 1L;
	Banco bc;

	public TelaListaClientes(String str, int tipoListagem) {
		super(str, false, true);

		bc = Banco.getInstance();
		
		String column[] = { "ID", "Nome", "Endere�o","Contato","�ltimo P.","Qtd P.","Ativo" };
		int contador = 0;
		ArrayList<Cliente> listaClientes;

		switch(tipoListagem) {
		  case 1:
			  listaClientes = bc.listaClientesAtivos();
			  break;
		  case 2:
			  listaClientes = bc.listaClientesBonificados();
			  break;
		  default:
			  listaClientes = bc.listaClientes();
			  break;
		}

		String clientes[][] = new String[listaClientes.size()][7];
		for (Cliente c : listaClientes) {
			String cliente[] = new String[7];
			cliente[0] = Integer.toString(c.getId());
			cliente[1] = c.getNome();
			cliente[2] = c.getEndereco();
			cliente[3] = c.getContato();

			if (c.getDataUltimoProcedimento() != null) {
				cliente[4] = DataUtilitarios.converteDataParaString(c.getDataUltimoProcedimento());
			} else {
				cliente[4] = "-";
			}
			
			cliente[5] = Integer.toString(c.getQuantidadeProcedimentos());
	
			if (c.EAtivo()) {
				cliente[6] = "Sim";
			} else {
				cliente[6] = "N�o";
			}
	
			clientes[contador] = cliente;
			contador += 1;
		}

		JTable jt=new JTable(clientes,column);    
		JScrollPane sp = new JScrollPane(jt);
		sp.setBounds(30,40,100,100);
		sp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		jt.setFillsViewportHeight(true); 

		Container ct = this.getContentPane();
		ct.setLayout(new FlowLayout());
		ct.add(sp);

		// especifica��es do formul�rio
		setSize(785, 600);
		setTitle(str);
	}
}
