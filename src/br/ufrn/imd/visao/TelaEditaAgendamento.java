package br.ufrn.imd.visao;

import java.awt.Container; 
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import br.ufrn.imd.modelo.Agendamento;
import br.ufrn.imd.modelo.Banco;
import br.ufrn.imd.utilitarios.DataUtilitarios;

public class TelaEditaAgendamento extends JInternalFrame implements ActionListener{
	
	private static final long serialVersionUID = 1L;
	
	Banco bc;
	
	// Format as datas
	SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
	
	// r�tulos
	JLabel lid = new JLabel("Id Agendamento.:");
	JLabel lprocedimentoId = new JLabel("Id Procedimento:");
	JLabel ldata = new JLabel("Data e Hora:");
	JLabel lprofissionalId = new JLabel("Id Profissional:");
	JLabel lclienteId = new JLabel("Id Cliente:");
			
	// campos
	JTextField tid = new JTextField();
	JTextField tprocedimentoId = new JTextField();
	JTextField tdata = new JTextField();
	JTextField thora = new JTextField();
	JTextField tprofissionalId = new JTextField();
	JTextField tclienteId = new JTextField();
			
	// bot�es
	JButton btBuscaId = new JButton("Buscar Id");
	JButton btSubmeter = new JButton("Editar");
	JButton btLimpar = new JButton("Limpar");

	public TelaEditaAgendamento(String str)  {
		super(str,false,true);
		
		Container ct = this.getContentPane();
		ct.setLayout(null);
		
			
		// coordenadas
		lid.setBounds(10,10,100,30);
		tid.setBounds(110,10,50,25);
		lprocedimentoId.setBounds(10,40,100,30);
		tprocedimentoId.setBounds(110,40,100,25);
		ldata.setBounds(10,70,100,30);
		tdata.setBounds(110,70,70,25);
		thora.setBounds(190,70,50,25);
		lprofissionalId.setBounds(10,100,100,30);
		tprofissionalId.setBounds(110,100,50,25);
		lclienteId.setBounds(10,130,100,30);
		tclienteId.setBounds(110,130,50,25);

		btBuscaId.setBounds(180,10,90,20);
		btSubmeter.setBounds(50,170,100,30);
		btLimpar.setBounds(230,170,100,30);
				
		// adicionando componentes
		ct.add(lid);
		ct.add(tid);
		ct.add(lprocedimentoId);
		ct.add(tprocedimentoId);
		ct.add(ldata);
		ct.add(tdata);
		ct.add(thora);
		ct.add(lprofissionalId);
		ct.add(tprofissionalId);
		ct.add(lclienteId);
		ct.add(tclienteId);
		ct.add(btBuscaId);
		ct.add(btSubmeter);
		ct.add(btLimpar);
		
		// evento dos bot�es
		btSubmeter.addActionListener(this);		
		btLimpar.addActionListener(this);
		btBuscaId.addActionListener(this);
		
		// especifica��es do formul�rio
		setSize(785, 600);
		setTitle(str);
		
		bc = Banco.getInstance();
	}
	
	public void limpaCampos() {
		tprocedimentoId.setText("");
		tdata.setText("");
		thora.setText("");
		tprofissionalId.setText("");
		tclienteId.setText("");
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btSubmeter){
			boolean run = true;
			try {
				if (bc.buscaCliente(Integer.parseInt(tclienteId.getText()))==null) {
					String mensagemInsucesso = "Cliente n�o encontrado";
					JOptionPane.showMessageDialog(this, mensagemInsucesso);
					run = false;
				}
				
				if (bc.buscaProcedimento(Integer.parseInt(tprocedimentoId.getText()))==null) {
					String mensagemInsucesso = "Procedimento n�o encontrado";
					JOptionPane.showMessageDialog(this, mensagemInsucesso);
					run = false;
				}
				if (bc.buscaProfissional(Integer.parseInt(tprofissionalId.getText()))==null) {
					String mensagemInsucesso = "Profissional n�o encontrado";
					JOptionPane.showMessageDialog(this, mensagemInsucesso);
					run = false;
				}
			} catch (Exception ept) {
				run = false;
				JOptionPane.showMessageDialog(this, "Existem Campos Com Valores inv�lidos");
			}

			if (run) {
				Agendamento a = new Agendamento();
				boolean deveAdd = true;
				try {
					a.setId(Integer.parseInt(tid.getText()));
					a.setClienteId(Integer.parseInt(tclienteId.getText()));
					a.setProcedimentoId(Integer.parseInt(tprocedimentoId.getText()));
					a.setProfissionalId(Integer.parseInt(tprofissionalId.getText()));
					a.setDataHora(tdata.getText(), thora.getText());
				} catch (Exception ept) {
					deveAdd = false;
					JOptionPane.showMessageDialog(this, "Existem campos com valores inv�lidos");
				}

				if (deveAdd) {
					String mensagemSucesso = bc.editaAgendamento(a);
					JOptionPane.showMessageDialog(this, mensagemSucesso);

					if (JOptionPane.OK_OPTION != 1) {
						this.limpaCampos();
					}
				}
			}
		} else if (e.getSource() == btLimpar) {
			limpaCampos();
		} else if (e.getSource() == btBuscaId) {
			Agendamento aAux = bc.buscaAgendamento(Integer.parseInt(tid.getText()));
			tprocedimentoId.setText(Integer.toString(aAux.getProcedimentoId()));
			String data = DataUtilitarios.converteDataHoraParaString(aAux.getDataHora());
			String[] partes = data.split(" ");
			tdata.setText(partes[0]);
			thora.setText(partes[1]);
			tprofissionalId.setText(Integer.toString(aAux.getProfissionalId()));
			tclienteId.setText(Integer.toString(aAux.getClienteId()));
		}
	}
}


