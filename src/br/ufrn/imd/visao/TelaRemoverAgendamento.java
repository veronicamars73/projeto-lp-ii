package br.ufrn.imd.visao;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import br.ufrn.imd.modelo.Agendamento;
import br.ufrn.imd.modelo.Banco;
import br.ufrn.imd.utilitarios.DataUtilitarios;

public class TelaRemoverAgendamento extends JInternalFrame implements ActionListener{
	
	private static final long serialVersionUID = 1L;
	
	Banco bc;
	
	// Format as datas
	SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
	
	// r�tulos
	JLabel lid = new JLabel("Id Agendamento.:");
	JLabel lprocedimentoId = new JLabel("Id Procedimento:");
	JLabel ldata = new JLabel("Data e Hora:");
	JLabel lprofissionalId = new JLabel("Id Profissional:");
	JLabel lclienteId = new JLabel("Id Cliente:");
			
	// campos
	JTextField tid = new JTextField();
			
	// bot�es
	JButton btBuscaId = new JButton("Buscar Id");
	JButton btSubmeter = new JButton("Deletar");

	public TelaRemoverAgendamento(String str)  {
		super(str,false,true);
		
		Container ct = this.getContentPane();
		ct.setLayout(null);
		
			
		// coordenadas
		lid.setBounds(10,10,100,30);
		tid.setBounds(110,10,50,25);
		lprocedimentoId.setBounds(10,40,200,30);
		ldata.setBounds(10,70,200,30);
		lprofissionalId.setBounds(10,100,200,30);
		lclienteId.setBounds(10,130,200,30);

		btBuscaId.setBounds(180,10,90,20);
		btSubmeter.setBounds(150,140,100,30);
				
		// adicionando componentes
		ct.add(lid);
		ct.add(tid);
		ct.add(lprocedimentoId);
		ct.add(ldata);
		ct.add(lprofissionalId);
		ct.add(lclienteId);
		ct.add(btBuscaId);
		ct.add(btSubmeter);
		
		// evento dos bot�es
		btSubmeter.addActionListener(this);	
		btBuscaId.addActionListener(this);
		
		// especifica��es do formul�rio
		setSize(390,210);
		setTitle(str);
		
		bc = Banco.getInstance();
	}
	

	
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btSubmeter) {
			String mensagemSucesso = bc.removeAgendamento(Integer.parseInt(tid.getText()));
			
			JOptionPane.showMessageDialog(this, mensagemSucesso);

			if (JOptionPane.OK_OPTION != 1) {
				tid.setText("");
			}
		} else if(e.getSource() == btBuscaId) {
			Agendamento aAux = bc.buscaAgendamento(Integer.parseInt(tid.getText()));
			if (aAux == null) {
				JOptionPane.showMessageDialog(this, "Agendamento n�o encontrado");
				lprocedimentoId.setText("Id Procedimento:");
				ldata.setText("Data e Hora:");
				lprofissionalId.setText("Id Profissional:");
				lclienteId.setText("Id Cliente:");
			} else {
				lprocedimentoId.setText("Id Procedimento:" + aAux.getProcedimentoId());
				ldata.setText("Data e Hora:" + DataUtilitarios.converteDataHoraParaString(aAux.getDataHora()));
				lprofissionalId.setText("Id Profissional:" + aAux.getProfissionalId());
				lclienteId.setText("Id Cliente:" + aAux.getClienteId());
			}
		}
	}
}


