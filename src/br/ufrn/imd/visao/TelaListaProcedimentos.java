package br.ufrn.imd.visao;

import java.awt.Container;
import java.awt.FlowLayout;
import java.util.ArrayList;

import javax.swing.JInternalFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import br.ufrn.imd.modelo.Banco;
import br.ufrn.imd.modelo.Procedimento;

public class TelaListaProcedimentos extends JInternalFrame {
	private static final long serialVersionUID = 1L;
	Banco bancoDeDados;
	
	TelaListaProcedimentos(String titulo) {
		super(titulo, false, true);
		
		bancoDeDados = Banco.getInstance();
		
		String colunas[] = { "ID", "Nome", "Duração em Min.", "Valor em R$" };
		ArrayList<Procedimento> procedimentos = bancoDeDados.listaProcedimentos();
		String listaProcedimentos[][] = new String[procedimentos.size()][4];
		int contador = 0;
		
		for (Procedimento procedimento: procedimentos) {
			String novoProcedimento[] = new String[4];
			
			novoProcedimento[0] = Integer.toString(procedimento.getId());
			novoProcedimento[1] = procedimento.getNome();
			novoProcedimento[2] = Integer.toString(procedimento.getDuracaoMinutos());
			novoProcedimento[3] = Double.toString(Math.round(procedimento.getValor()));
			
			listaProcedimentos[contador] = novoProcedimento;
			contador += 1;
		}
		
		JTable table = new JTable(listaProcedimentos, colunas);   	
		JScrollPane scrollPane = new JScrollPane(table);
	
		scrollPane.setBounds(30,40,100,100);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		table.setFillsViewportHeight(true); 

		Container container = this.getContentPane();
		container.setLayout(new FlowLayout());
		container.add(scrollPane);

		// especificações do formulário
		setSize(785, 600);
		setTitle(titulo);

	}
}
