package br.ufrn.imd.visao;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import br.ufrn.imd.modelo.Procedimento;
import br.ufrn.imd.modelo.Banco;
import br.ufrn.imd.modelo.Profissional;

public class TelaCadastroProfissional extends JInternalFrame implements ActionListener{
	
	private static final long serialVersionUID = 1L;

	Banco banco;
	
	// Format as datas
	SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
	
	// r�tulos
	JLabel lnome = new JLabel("Nome:");
	JLabel lcontato = new JLabel("Contato:");
	JLabel ldatainicio = new JLabel("Data de In�cio:");
	JLabel lprocedimentos = new JLabel("Procedi. (sep;):");
	
			
	// campos
	JTextField tnome = new JTextField();
	JTextField tcontato = new JTextField();
	JTextField tdatainicio = new JTextField();
	JTextField tprocedimentos= new JTextField();
			
	// bot�es
	JButton btSubmeter = new JButton("Cadastrar");
	JButton btLimpar = new JButton("Limpar");

	public TelaCadastroProfissional(String str) {
		super(str,false,true);
		
		Container ct = this.getContentPane();
		ct.setLayout(null);
		
			
		// coordenadas
		lnome.setBounds(10,10,100,30);
		tnome.setBounds(100,10,280,25);
		lcontato.setBounds(10,40,100,30);
		tcontato.setBounds(100,40,100,25);
		ldatainicio.setBounds(10,70,100,30);
		tdatainicio.setBounds(100,70,70,25);
		lprocedimentos.setBounds(10,100,100,30);
		tprocedimentos.setBounds(100,100,280,25);

		// idem
		btSubmeter.setBounds(50,140,100,30);
		btLimpar.setBounds(230,140,100,30);
				
		// adicionando componentes
		ct.add(lnome);
		ct.add(tnome);
		ct.add(lcontato);
		ct.add(tcontato);
		ct.add(ldatainicio);
		ct.add(tdatainicio);
		ct.add(lprocedimentos);
		ct.add(tprocedimentos);
		ct.add(btSubmeter);
		ct.add(btLimpar);
		
		// evento dos bot�es
		btSubmeter.addActionListener(this);		
		btLimpar.addActionListener(this);
		
		// especifica��es do formul�rio
		setSize(785, 600);
		setTitle(str);
		
		banco = Banco.getInstance();
	}
	
	public void limpaCampos() {
		tnome.setText("");
		tcontato.setText("");
		tdatainicio.setText("");
		tprocedimentos.setText("");
	}
	
	
	@Override
	public void actionPerformed(ActionEvent evento) {
		if (evento.getSource() == btSubmeter) {
			Profissional profissional = new Profissional();
			boolean deveAdd = true;
			String mensagemErro = "";

			try {
				
				if (tnome.getText().equals("")) {
					deveAdd = false;
					mensagemErro = "O campo Nome n�o pode ser vazio \n";
				} else {
					profissional.setNome(tnome.getText());
				}
				
				if (tcontato.getText().equals("")) {
					deveAdd = false;
					mensagemErro += "O campo Contato n�o pode ser vazio \n";
				} else {
					profissional.setContato(tcontato.getText());
				}

				profissional.setDataInicioTrabalho(tdatainicio.getText());

				String[] procedimentos = (tprocedimentos.getText()).split(";");
				
				
				for (String procedimento : procedimentos) {
					// busca procedimento por nome e verifica se existe no banco de dados
					// se n�o existir, mostra mensagem de alerta
					Procedimento procedimentoBuscado = banco.buscaProcedimento(procedimento);
					if (procedimentoBuscado != null) {
						profissional.addProcedimentosAplicaveis(procedimentoBuscado);
					} else {
						deveAdd = false;
						JOptionPane.showMessageDialog(this, "Procedimento '"+ procedimento + "' n�o encontrado.");
						break;
					}
				}
				
			} catch (Exception ept) {
				deveAdd = false;
				mensagemErro += "Existem campos com valores inv�lidos! \nPor favor, "
						+ "verifique as informa��es preenchidas";
			} finally {
				if (deveAdd) {
					String mensagemSucesso = banco.addProfissional(profissional);
					JOptionPane.showMessageDialog(this, mensagemSucesso);

					if (JOptionPane.OK_OPTION != 1) {
						this.limpaCampos();
					}
				} else {
					if (!mensagemErro.equals("")) {
						JOptionPane.showMessageDialog(this, mensagemErro);
					}
				}
			}
		} else if (evento.getSource() == btLimpar) {
			this.limpaCampos();
		}
	}
}


