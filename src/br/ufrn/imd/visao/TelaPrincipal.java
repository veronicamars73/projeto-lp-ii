package br.ufrn.imd.visao;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import br.ufrn.imd.modelo.Agendamento;
import br.ufrn.imd.modelo.Cliente;
import br.ufrn.imd.modelo.Procedimento;
import br.ufrn.imd.modelo.Profissional;

public class TelaPrincipal extends JFrame implements ActionListener{
	
	private static final long serialVersionUID = 1L;

	JDesktopPane janelaPrincipal = new JDesktopPane();
	
	JMenuBar barraMenu = new JMenuBar();	

	JMenu menuCadastros = new JMenu("Cadastrar");
	JMenu menuListas = new JMenu("Visualizar");
	JMenu menuEdicoes = new JMenu("Editar");
	JMenu menuRemocoes = new JMenu("Remover");
	JMenu menuAjuda = new JMenu("Ajuda");
	
	JMenuItem opcaoCadastroProcedimento = new JMenuItem("Procedimento");
	JMenuItem opcaoCadastroCliente = new JMenuItem("Cliente");
	JMenuItem opcaoCadastroProfissional = new JMenuItem("Profissional");
	JMenuItem opcaoCadastroAgendamento = new JMenuItem("Agendamento");
	
	JMenuItem opcaoListaProcedimentos = new JMenuItem("Lista de Procedimentos");
	
	JMenu menuListaClientes = new JMenu("Lista de Clientes");
	JMenuItem opcaoListaClientesBonificados = new JMenuItem("Bonificados");  
	JMenuItem opcaoListaClientesAtivos = new JMenuItem("Ativos");
	JMenuItem opcaoListaTodosClientes = new JMenuItem("Todos");

	JMenu menuListaProfissionais = new JMenu("Lista de Profissionais");
	JMenuItem opcaoListaProfBonificados = new JMenuItem("Bonificados");  
	JMenuItem opcaoListaProfAtivos = new JMenuItem("Ativos");
	JMenuItem opcaoListaTodosProf = new JMenuItem("Todos");
	
	JMenu menuListaAgendamentos = new JMenu("Lista de Agendamentos");
	JMenuItem opcaoAgendamentosDoDia = new JMenuItem("Agendamentos do Dia");  
	JMenuItem opcaoProximosAgendamentos = new JMenuItem("Pr�ximos Agendamentos");
	JMenuItem opcaoTodosAgendamentos = new JMenuItem("Todos");
	
	
	JMenuItem opcaoEditaProcedimento = new JMenuItem("Procedimento");
	JMenuItem opcaoEditaAgendamento = new JMenuItem("Agendamento");
	
	JMenuItem opcaoRemoveProcedimento = new JMenuItem("Procedimento");
	JMenuItem opcaoRemoveAgendamento = new JMenuItem("Agendamento");
	
	JMenuItem opcaoSair = new JMenuItem("Sair");
		 		
	public TelaPrincipal(){
		Container container = this.getContentPane();
		container.setLayout(new BorderLayout());
		
		setJMenuBar(barraMenu);
		barraMenu.add(menuCadastros);
		barraMenu.add(menuListas);
		barraMenu.add(menuEdicoes);
		barraMenu.add(menuRemocoes);
		barraMenu.add(menuAjuda);

		menuCadastros.add(opcaoCadastroProcedimento);
		menuCadastros.add(opcaoCadastroCliente);
		menuCadastros.add(opcaoCadastroProfissional);
		menuCadastros.add(opcaoCadastroAgendamento);
		
		
		menuListaClientes.add(opcaoListaClientesAtivos);
		menuListaClientes.add(opcaoListaClientesBonificados);
		menuListaClientes.add(opcaoListaTodosClientes);
		menuListaProfissionais.add(opcaoListaProfAtivos);
		menuListaProfissionais.add(opcaoListaProfBonificados);
		menuListaProfissionais.add(opcaoListaTodosProf);
		menuListaAgendamentos.add(opcaoAgendamentosDoDia);
		menuListaAgendamentos.add(opcaoProximosAgendamentos);
		menuListaAgendamentos.add(opcaoTodosAgendamentos);
		
	
		menuListas.add(menuListaClientes);
		menuListas.add(menuListaProfissionais);
		menuListas.add(menuListaAgendamentos);
		menuListas.add(opcaoListaProcedimentos);
		
		menuEdicoes.add(opcaoEditaProcedimento);
		menuEdicoes.add(opcaoEditaAgendamento);
		
		menuRemocoes.add(opcaoRemoveProcedimento);
		menuRemocoes.add(opcaoRemoveAgendamento);
		
		menuAjuda.add(opcaoSair);
		
		container.add(BorderLayout.CENTER, janelaPrincipal);
		
		setSize(800, 600);
		setResizable(false);
		setTitle("SIG Beauty - Sistema Gerenciador de Procedimentos de Beleza");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		// eventos
		opcaoCadastroProcedimento.addActionListener(this);	
		opcaoCadastroCliente.addActionListener(this);
		opcaoCadastroProfissional.addActionListener(this);
		opcaoCadastroAgendamento.addActionListener(this);
		
		opcaoListaClientesAtivos.addActionListener(this);
		opcaoListaClientesBonificados.addActionListener(this);
		opcaoListaTodosClientes.addActionListener(this);
		
		opcaoListaProfAtivos.addActionListener(this);
		opcaoListaProfBonificados.addActionListener(this);
		opcaoListaTodosProf.addActionListener(this);
		
		opcaoAgendamentosDoDia.addActionListener(this);
		opcaoProximosAgendamentos.addActionListener(this);
		opcaoTodosAgendamentos.addActionListener(this);
		
		opcaoListaProcedimentos.addActionListener(this);
		
		opcaoEditaProcedimento.addActionListener(this);
		opcaoEditaAgendamento.addActionListener(this);
		
		opcaoRemoveProcedimento.addActionListener(this);
		opcaoRemoveAgendamento.addActionListener(this);
		
		opcaoSair.addActionListener(this);
		
		
	}
	
	public void mostraMensagem(String modulo) {
		JOptionPane.showMessageDialog(this, "N�o existem " + modulo + " cadastrados.");
	}
	
	@Override
	public void actionPerformed(ActionEvent evento) {
		if (evento.getSource() == opcaoCadastroCliente){
			TelaCadastroClientes telaCliente = new TelaCadastroClientes("Clientes");
			janelaPrincipal.add(telaCliente);
			telaCliente.setVisible(true);
		}
		
		if (evento.getSource() == opcaoCadastroProfissional){
			TelaCadastroProfissional telaProfissional = new TelaCadastroProfissional("Profissionais");
			janelaPrincipal.add(telaProfissional);
			telaProfissional.setVisible(true);
		}
		
		if (evento.getSource() == opcaoCadastroProcedimento){
			TelaCadastroProcedimento telaProcedimento = new TelaCadastroProcedimento("Procedimentos");
			janelaPrincipal.add(telaProcedimento);
			telaProcedimento.setVisible(true);
		}
	
		if (evento.getSource() == opcaoCadastroAgendamento) {
			TelaCadastroAgendamento telaAgendamento = new TelaCadastroAgendamento("Agendamento");
			janelaPrincipal.add(telaAgendamento);
			telaAgendamento.setVisible(true);
		}
		
		if (evento.getSource() == opcaoListaClientesAtivos) {
			TelaListaClientes telaCliente = new TelaListaClientes("Clientes", 1);
			ArrayList<Cliente> clientes = telaCliente.bc.listaClientesAtivos();

			if (clientes.size() == 0) {
				this.mostraMensagem("clientes ativos");
			} else {
				janelaPrincipal.add(telaCliente);
				telaCliente.setVisible(true);
			}
		}
		
		if (evento.getSource() == opcaoListaClientesBonificados) {
			TelaListaClientes telaCliente = new TelaListaClientes("Clientes", 2);
			ArrayList<Cliente> clientes = telaCliente.bc.listaClientesBonificados();

			if (clientes.size() == 0) {
				this.mostraMensagem("clientes bonificados");
			} else {
				janelaPrincipal.add(telaCliente);
				telaCliente.setVisible(true);
			}
		}
	
		if (evento.getSource() == opcaoListaTodosClientes) {
			TelaListaClientes telaCliente = new TelaListaClientes("Clientes", 3);
			ArrayList<Cliente> clientes = telaCliente.bc.listaClientes();

			if (clientes.size() == 0) {
				this.mostraMensagem("clientes");
			} else {
				janelaPrincipal.add(telaCliente);
				telaCliente.setVisible(true);
			}
		}
		
		if (evento.getSource() == opcaoListaProfBonificados) {
			TelaListaProfissionais telaProfissional = new TelaListaProfissionais("Profissionais", 1);
			ArrayList<Profissional> profissionais = telaProfissional.bancoDeDados.listaProfissionaisBonificados();
			
			if (profissionais.size() == 0) {
				this.mostraMensagem("profissionais bonificados");
			} else {
				janelaPrincipal.add(telaProfissional);
				telaProfissional.setVisible(true);
			}
		}
		
		if (evento.getSource() == opcaoListaProfAtivos) {
			TelaListaProfissionais telaProfissional = new TelaListaProfissionais("Profissionais", 2);
			ArrayList<Profissional> profissionais = telaProfissional.bancoDeDados.listaProfissionaisAtivos();
			
			if (profissionais.size() == 0) {
				this.mostraMensagem("profissionais ativos");
			} else {
				janelaPrincipal.add(telaProfissional);
				telaProfissional.setVisible(true);
			}
		}

		if (evento.getSource() == opcaoListaTodosProf) {
			TelaListaProfissionais telaProfissional = new TelaListaProfissionais("Profissionais", 3);
			ArrayList<Profissional> profissionais = telaProfissional.bancoDeDados.listaProfissionais();
			
			if (profissionais.size() == 0) {
				this.mostraMensagem("profissionais");
			} else {
				janelaPrincipal.add(telaProfissional);
				telaProfissional.setVisible(true);
			}
		}
		
		if (evento.getSource() == opcaoListaProcedimentos) {
			TelaListaProcedimentos telaProcedimento = new TelaListaProcedimentos("Procedimentos");
			ArrayList<Procedimento> procedimentos = telaProcedimento.bancoDeDados.listaProcedimentos();
			
			if (procedimentos.size() == 0) {
				this.mostraMensagem("procedimentos");
			} else {
				janelaPrincipal.add(telaProcedimento);
				telaProcedimento.setVisible(true);
			}
		}
		
		if (evento.getSource() == opcaoProximosAgendamentos) {
			TelaListaAgendamentos telaAgendamento = new TelaListaAgendamentos("Pr�ximos Agendamento", 2);
			ArrayList<Agendamento> agendamentos = telaAgendamento.bancoDeDados.listaAgendamentos();
			
			if (agendamentos.size() == 0) {
				this.mostraMensagem("agendamentos");
			} else {
				janelaPrincipal.add(telaAgendamento);
				telaAgendamento.setVisible(true);
			}
		}
		
		if (evento.getSource() == opcaoTodosAgendamentos) {
			TelaListaAgendamentos telaAgendamento = new TelaListaAgendamentos("Todos os Agendamentos", 3);
			ArrayList<Agendamento> agendamentos = telaAgendamento.bancoDeDados.listaAgendamentos();
			
			if (agendamentos.size() == 0) {
				this.mostraMensagem("agendamentos");
			} else {
				janelaPrincipal.add(telaAgendamento);
				telaAgendamento.setVisible(true);
			}
		}
		
		if (evento.getSource() == opcaoAgendamentosDoDia) {
			TelaListaAgendamentos telaAgendamento = new TelaListaAgendamentos("Agendamentos do Dia", 1);
			ArrayList<Agendamento> agendamentos = telaAgendamento.bancoDeDados.listaAgendamentosDoDia();
			
			if (agendamentos.size() == 0) {
				this.mostraMensagem("agendamentos do dia");
			} else {
				janelaPrincipal.add(telaAgendamento);
				telaAgendamento.setVisible(true);
			}
		}
	
		if (evento.getSource() == opcaoEditaProcedimento) {
			TelaEditaProcedimento telaProcedimento = new TelaEditaProcedimento("Procedimento");
			janelaPrincipal.add(telaProcedimento);
			telaProcedimento.setVisible(true);
		}

		if (evento.getSource() == opcaoEditaAgendamento) {
			TelaEditaAgendamento telaAgendamento = new TelaEditaAgendamento("Agendamento");
			janelaPrincipal.add(telaAgendamento);
			telaAgendamento.setVisible(true);
		}

		if (evento.getSource() == opcaoRemoveProcedimento) {
			TelaRemoverProcedimento telaProcedimento = new TelaRemoverProcedimento("Procedimento");
			janelaPrincipal.add(telaProcedimento);
			telaProcedimento.setVisible(true);
		}

		if (evento.getSource() == opcaoRemoveAgendamento) {
			TelaRemoverAgendamento telaAgendamento = new TelaRemoverAgendamento("Agendamento");
			janelaPrincipal.add(telaAgendamento);
			telaAgendamento.setVisible(true);
		}
		
		if (evento.getSource() == opcaoSair) {
			System.exit(0);
		}
	}
	
	public static void main(String[] args) {
		TelaPrincipal telaPai = new TelaPrincipal();
		telaPai.setVisible(true);
	} 

}

