package br.ufrn.imd.visao;

import java.awt.Container;
import java.awt.FlowLayout;
import java.util.ArrayList;

import javax.swing.JInternalFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

import br.ufrn.imd.modelo.Banco;
import br.ufrn.imd.modelo.Procedimento;
import br.ufrn.imd.modelo.Profissional;
import br.ufrn.imd.utilitarios.DataUtilitarios;

public class TelaListaProfissionais extends JInternalFrame {
	private static final long serialVersionUID = 1L;
	Banco bancoDeDados;
	
	public TelaListaProfissionais(String titulo, int tipoListagem) {
		super(titulo, false, true);
		bancoDeDados = Banco.getInstance();
		
		String colunas[] = { "ID", "Nome", "Contato", "Data de In�cio de Trabalho", "Proc. Aplic�veis", "Ativo" };
		
		ArrayList<Profissional> profissionais;

		switch(tipoListagem) {
		  case 1:
			  profissionais = bancoDeDados.listaProfissionaisBonificados();
			  break;
		  case 2:
			  profissionais = bancoDeDados.listaProfissionaisAtivos();
			  break;
		  default:
			  profissionais = bancoDeDados.listaProfissionais();
			  break;
		}
		
		String listaProfissionais[][] = new String[profissionais.size()][6];
		int contador = 0;

		for (Profissional profissional: profissionais) {
			String novoProfissional[] = new String[6];
			
			novoProfissional[0] =  Integer.toString(profissional.getId());
			novoProfissional[1] = profissional.getNome();
			novoProfissional[2] = profissional.getContato();
			
			if (profissional.getDataInicioTrabalho() != null) {
				novoProfissional[3] = DataUtilitarios.converteDataParaString(profissional.getDataInicioTrabalho());
			} else {
				novoProfissional[3] = "-";
			}
			
			String procedimentos = "";
			ArrayList<Procedimento> listaDeProcedimentos = profissional.getProcedimentosAplicaveis();

			for (Procedimento procedimento: listaDeProcedimentos) {
				procedimentos+= procedimento.getNome() + ", ";
			}
			procedimentos = procedimentos.substring(0, procedimentos.length() - 2);
			
			novoProfissional[4] = procedimentos;
			
			if (profissional.EAtivo()) {
				novoProfissional[5] = "Sim";
			} else {
				novoProfissional[5] =  "N�o";
			}
			
			listaProfissionais[contador] = novoProfissional;
			contador += 1;
		}
		
		JTable table = new JTable(listaProfissionais, colunas);   	
		JScrollPane scrollPane = new JScrollPane(table);
	
		scrollPane.setBounds(30,40,100,100);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		table.setFillsViewportHeight(true); 

		Container container = this.getContentPane();
		container.setLayout(new FlowLayout());
		container.add(scrollPane);

		// especifica��es do formul�rio
		setSize(785, 600);
		setTitle(titulo);
	}
}
