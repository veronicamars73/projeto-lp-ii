package br.ufrn.imd.utilitarios;

import java.util.Date;
import java.util.Calendar;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class DataUtilitarios {
	
	public DataUtilitarios() {}
	
	public static Date converteStringParaData(String data, String hora) throws ParseException {	
		SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy HH:mm");

		String dataHora = data + " " + hora;
	    Date dataHoraConvertida = null;

		dataHoraConvertida = formatador.parse(dataHora);
		

		return dataHoraConvertida;
    }
	
	public static Date converteStringParaData(String data) throws ParseException {
		SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
	    Date dataConvertida = null;

		dataConvertida = formatador.parse(data);
		return dataConvertida;
	}
	
	public static String converteDataParaString(Date data) {
		SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy");
	    String dataFormatada = formatador.format(data);

		return dataFormatada;
	}
	
	public static String converteDataHoraParaString(Date data) {
		SimpleDateFormat formatador = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	    String dataFormatada = formatador.format(data);

		return dataFormatada;
	}

	
	public static boolean eAnteriorAPeriodo(Date data, int quantidadeMeses) {
		int periodo = quantidadeMeses;
		Date dataAtual = new Date();
		Calendar calendario = Calendar.getInstance();

		calendario.setTime(dataAtual);
		calendario.add(Calendar.MONTH, -periodo);
       
        Date dataPeriodo = calendario.getTime();
        try {
	        if (data.before(dataPeriodo)) {
	        	return true;
	        }
        }catch (NullPointerException nexc){
        	return true;
        }
        return false;
	}
	
}
